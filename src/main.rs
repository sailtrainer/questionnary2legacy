use serde::{Deserialize, Serialize};
use std::env;
use std::fs::File;
use std::io::BufReader;
use quick_xml::escape::escape;
use uuid::Uuid;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Questionnaire {
    topics: Vec<Topic>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Topic {
    topic_id: Uuid,
    legacy_id: String,
    name: String,
    questions: Vec<Question>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Question {
    question_id: Uuid,
    legacy_id: String,
    reference: String,
    text: String,
    correct: String,
    incorrect: Vec<String>,
}

fn main() {
    let json_file = env::args().skip(1).next().expect("inputfile.json");

    let file = File::open(json_file).expect("File not found");
    let reader = BufReader::new(file);
    let questionnaire: Questionnaire = serde_json::from_reader(reader).expect("JSON content");

    println!("<?xml version='1.0' standalone='yes'?>");
    println!("<questionaire xmlns='http://sportboot.mobi/'>");

    for topic in questionnaire.topics {
        println!("<topic id='{}' name='{}'>", escape(&topic.legacy_id), escape(&topic.name));

        for question in topic.questions {
            println!("  <question id='{}' reference='{}'>", escape(&question.legacy_id), escape(&question.reference));
            println!("    <text>{}</text>", escape(&question.text));
            println!("    <correct>{}</correct>", escape(&question.correct));
            for incorrect in question.incorrect {
                println!("    <incorrect>{}</incorrect>", escape(&incorrect));
            }
            println!("  </question>");
        }

        println!("</topic>");
    }

    println!("</questionaire>");
}
